;-----------------------
; abs(int n)
;   @param
;       rdi = n
;
;   @return rax
;       |n|
;-----------------------
global vabs
vabs:
    mov rax, rdi
    neg rax
    cmovl rax, rdi
    ret
