;-----------------------
; strlen(char *str)
;   @param
;   rdi = str
;
;   @return rax
;       size of str
;-----------------------
global strlen
strlen:
    mov rax, rdi
    .nextchar:
        cmp byte[rax], 0
        jz .return
        inc rax
        jmp .nextchar
    .return:
        sub rax, rdi
        ret

;-----------------------
; void reverse(char *str)
;   @param
;       rdi = str
;   
;   @pros
;       reverse str
;-----------------------
global reverse
reverse:
    push rax
    push rbx
    push rcx
    call strlen
    mov rbx, rdi
    add rbx, rax
    dec rbx
    mov rax, rdi
    .swap:
        cmp rax, rbx
        jge .end
        mov ch, [rax]
        mov cl, [rbx]
        mov [rbx], ch
        mov [rax], cl
        inc rax
        dec rbx
        jmp .swap
    .end:
        pop rcx
        pop rbx
        pop rax
        ret

;-----------------------
; int void atoi(char *str, int base)
;   @param
;       rdi = str
;       rsi = base
;
;   @return rax
;       Transformation of an str into a int
;       in base (rdx)
;-----------------------
global atoi
atoi:
    push rdi
    push rbx
    xor rax, rax
    .add_unit:
        cmp [rdi], byte 0
        je .exit
        mov ebx, esi
        mul ebx
        xor ebx, ebx
        mov bl, [rdi]
        sub ebx, '0'
        add eax, ebx
        inc rdi
        jmp .add_unit
    .exit:
        pop rdi
        pop rbx
        ret
    .error:
        pop rbx
        pop rdi
        ret

;-----------------------
; itoa(int num, char *str, int base)
;   @param
;       rsi = num
;       rdi = str
;       rdx = base
;
;   @pros
;       Transformation of an int into a string 
;       in base (rsi)
;-----------------------
global itoa
itoa:
    push rax
    push rcx
    push rdi
    push rsi
    .loopConvert:
        cmp rsi, 0
        je .exit
        push rdx
        mov eax, esi
        mov ecx, edx
        mov edx, 0
        div ecx
        mov esi, eax
        mov eax, edx
        pop rdx
        add eax, '0'
        mov [rdi], eax
        inc rdi
        jmp .loopConvert
    .exit:
        mov byte [rdi], byte 0
        pop rsi
        pop rdi
        push rsi
        mov rsi, rdi
        call reverse
        mov rdi, rsi
        pop rsi
        pop rcx
        pop rax
        ret

;-----------------------
; int strcmp(char *str1, char *str2)
;   @param
;       rdi = str1
;       rsi = str2
;
;   @return rax
;       -1 str1 < str2
;        0 str1 == str2
;        1 str1 > str2
;-----------------------
global strcmp
strcmp:
    push rdi
    push rsi
    .cmpLoop:
        .checkRDIend:
            cmp [rdi], byte 0
            jne .checkRSIend
            cmp [rsi], byte 0
            je .equalEnd
            mov rax, -1
            jmp .exit
            .equalEnd:
                mov rax, 0
                jmp .exit
        .checkRSIend:
            cmp [rsi], byte 0
            jne .cmpChar
        .cmpChar:
            mov ah, [rdi]
            cmp ah, [rsi]
            je .equal
            jl .lower
            .bigger:
                mov rax, 1
                jmp .exit
            .lower:
                mov rax, -1
                jmp .exit
            .equal:
                inc rdi
                inc rsi
                jmp .cmpLoop
    .exit:
        pop rsi
        pop rdi
        ret

;-----------------------
; char* strcmp(char *str, char *substr)
;   @param
;       rdi = str
;       rsi = substr
;
;   @return rax
;           If string contains substring, the return value is the
;       location of the first matching instance of substring
;       in string.  If string doesn't contain substring, the
;       return value is 0
;-----------------------
global strstr
strstr:
    push rdi
    cmp rsi, 0
    jne .loopStr
    mov rax, rdi
    ret
    .loopStr:
        mov ah, [rdi]
        cmp ah, [rsi] 
        je .loopsetup
        inc rdi
        cmp ah, 0
        jne .loopStr
        pop rdi
        mov rax, 0
        ret
        .loopsetup:
            push rdi
            push rsi
        .loopSubStr:
            inc rdi
            inc rsi
            mov ah, [rdi]
            cmp [rsi], byte 0
            je .find
            cmp ah, byte [rsi]
            je .equal
            inc rdi
            jmp .loopStr
            .equal:
                jmp .loopSubStr
            .find:
                pop rsi
                pop rdi
                mov rax, rdi
                jmp .return
    .return:
        pop rdi
        ret