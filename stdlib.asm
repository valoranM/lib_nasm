;-----------------------
; exit(int ret)
;   @param
;       rdi = ret
;
;   @pros
;       exit program with ret code
;-----------------------
global exit
exit:
    mov rax, 1
    mov rbx, rdi
    int 80H