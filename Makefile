NASM= nasm
NASM_FORMAT= elf64

LD= ld
LD_FORMAT= elf_x86_64

SRCS= $(wildcard *.asm)
OBJ= $(SRCS:.asm=.o)
LIBS= $(SRCS:.asm=.so)
LIBS:= $(addprefix bin/, $(LIBS))

TESTS= $(wildcard test/*.asm)
TESTS:= $(TESTS:.asm=)

.SUFFIXES: .asm .o .so
.DEFAULT:

.PHONY: all
all: $(LIBS) $(TESTS)

%.o: %.asm
	@$(NASM) -f $(NASM_FORMAT) -o $@ $<
	 
bin/%.so: %.o
	@$(LD) -r -m $(LD_FORMAT) -o $@ $^
	@echo "update $(@F)"
	@rm -f $<

# Compile all libs

bin/stdio.so: stdio.o bin/string.so

# Compile all test executable

$(TESTS):
	@echo "Compiling test $@.asm"
	@$(LD) -m $(LD_FORMAT) -o bin_test/$(@F) $^
	@rm -f $@.o

test/stdio: bin/stdio.so bin/stdlib.so test/stdio.o

test/string: bin/stdio.so bin/stdlib.so test/string.o

test/math: bin/stdio.so bin/stdlib.so bin/math.so test/math.o

# Cleanup

.PHONY: clean, rmproper
clean: rmproper
	@rm -f ./{bin,test}/*{.so,.o} ./bin_test/*

rmproper:
	@rm -f ./{bin,test}/*{.o} *.o
