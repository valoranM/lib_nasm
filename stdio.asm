extern strlen
;-----------------------
; print(char *str)
;   @param
;       rdi = str
;
;   @pros
;       print str
;-----------------------
global print
print:
    push rax
    push rbx
    push rcx
    call strlen
    mov rdx, rax
    mov rax, 4
    mov rbx, 1
    mov rcx, rdi
    int 80h
    pop rcx
    pop rbx
    pop rax
    ret