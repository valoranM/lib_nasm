    extern vabs
    extern exit
    extern print

section .data

text: db "OK", 10, 0

section .text

    global _start

_start:
    mov rdi, 5
    neg rdi
    call vabs
    cmp rax, 5
    jne exit
    mov rdi, text
    call print
    mov rdi, 5
    call vabs
    cmp rax, 5
    jne exit
    mov rdi, text
    call print
    .exit:
        call exit