    extern print
    extern strlen
    extern reverse
    extern atoi
    extern itoa
    extern exit
    extern strcmp
    extern strstr

section .data

strlenData:
    .strlenF:    db "   -> Failure strlen()",10,0
    .strlenT:    db "strlen Function Test", 10, 0
    .test1:      db "test1",0
    .test1L:     equ $-.test1
    .test2:      db "test2",10,0
    .test2L:     equ $-.test2

revData:
    .revF:      db "   -> Failure reverse()",10,0
    .revT:      db "reverse Function Test", 10, 0
    .test1:     db "abcd",0
    .test1V:    db "dcba", 0

atoiData:
    .atoiF:     db "   -> Failure atoi()",10,0
    .atoiT:     db "atoi Function Test", 10, 0
    .test1:     db "10",0
    .test2:     db "10",0

itoaData:
    .atoiF:     db "   -> Failure itoa()\n",0
    .itoaT:     db "itoa Function Test", 10, 0
    .test1:     times 10 db ""
    .test1V:    db "10", 0

strcmpData:
    .cmpF:      db "   -> Failure strlen()",10,0
    .cmpT:      db "strcmp Function Test", 10, 0
    .empty:     db 0
    .test1:     db "abc", 0
    .test2:     db "abcd", 0
    .test3:     db "abc", 0

strstrData:
    .strstrF:   db "   -> Failure strstr()",10,0
    .strstrT:   db "strstr Function Test", 10, 0
    .test11:    db "abcdef", 10, 0
    .test12:    db "bcd", 0

section .text

    global _start

_start:
    call test_strlen
    call test_reverse
    call test_atoi
    call test_itoa
    call test_strcmp
    call test_strstr
    .exit:
        call exit

test_strlen:
    mov rdi, strlenData.test1
    call strlen
    mov rcx, strlenData.test1L
    dec rcx
    cmp rax, rcx
    jnz .fail_strlen

    mov rdi, strlenData.test2
    call strlen
    mov rcx, strlenData.test2L
    dec rcx
    cmp rax, rcx
    jnz .fail_strlen

    mov rdi, strlenData.strlenT
    call print
    jmp .exit
    .fail_strlen:
        call print
        mov rdi, strlenData.strlenF
        call print
    .exit:
        ret

test_reverse:
    mov rdi, revData.revT
    call print
    mov rdi, revData.test1
    call reverse
    mov rsi, revData.test1V
    call strcmp
    cmp rax, 0
    je .exit
    mov rdi, revData.revF
    call print
    .exit:
        ret

test_atoi:
    mov rdi, atoiData.atoiT
    call print
    .test1:
        mov rdi, atoiData.test1
        mov rsi, 10
        call atoi
        cmp eax, 10
        je .test2
        mov rdi, atoiData.atoiF
        call print
    .test2:
        mov rdi, atoiData.test2
        mov rsi, 2
        call atoi
        cmp eax, 2
        je .exit
        mov rdi, atoiData.atoiF
        call print
    .exit:
        ret

test_itoa:
    mov rdi, itoaData.itoaT
    call print
    mov rsi, 10
    mov rdi, itoaData.test1
    mov rdx, 10
    call itoa
    mov rdi, itoaData.test1
    mov rsi, itoaData.test1V
    call strcmp
    cmp rax, 0
    je .exit
    mov rdi, itoaData.atoiF
    call print
    .exit:
        ret


test_strcmp:
    mov rdi, strcmpData.cmpT
    call print
    .test1:
        mov rdi, strcmpData.empty
        mov rsi, strcmpData.empty
        call strcmp
        cmp rax, 0
        je .test2
        mov rdi, strcmpData.cmpF
        call print
    .test2:
        mov rdi, strcmpData.empty
        mov rsi, strcmpData.test1
        call strcmp
        cmp rax, -1
        je .test3
        mov rdi, strcmpData.cmpF
        call print
    .test3:
        mov rdi, strcmpData.test1
        mov rsi, strcmpData.empty
        call strcmp
        cmp rax, 1
        je .test4
        mov rdi, strcmpData.cmpF
        call print
    .test4:
        mov rdi, strcmpData.test1
        mov rsi, strcmpData.test2
        call strcmp
        cmp rax, -1
        je .test5
        mov rdi, strcmpData.cmpF
        call print
    .test5:
        mov rdi, strcmpData.test2
        mov rsi, strcmpData.test3
        call strcmp
        cmp rax, -1
        je .test6
        mov rdi, strcmpData.cmpF
        call print
    .test6:
        mov rdi, strcmpData.test3
        mov rsi, strcmpData.test2
        call strcmp
        cmp rax, 1
        je .exit
        mov rdi, strcmpData.cmpF
        call print
    .exit:
        ret

test_strstr:
    mov rdi, strstrData.strstrT
    call print
    mov rdi, strstrData.test11
    mov rsi, strstrData.test12
    call strstr
    inc rdi
    cmp rax, rdi
    je .exit
    mov rdi, strstrData.strstrF
    call print
    ret
    .exit:
        ret