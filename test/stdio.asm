    extern print
    extern exit

section .data

msg:    db "salut",0

section .text

    global _start

_start:
    mov rdi, msg
    call print
    call exit